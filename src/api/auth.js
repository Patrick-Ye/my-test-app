import axios from 'axios';

export function login(email, password) {
  return axios.post('/auth', {email, password})
  .then(auth => {
    const token = auth.data.data;
    localStorage.setItem('jwt_token', token);
    axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.jwt_token}`;
    return token;
  });
}